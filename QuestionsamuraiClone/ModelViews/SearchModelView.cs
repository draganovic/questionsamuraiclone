﻿using System.ComponentModel.DataAnnotations;

namespace QuestionsamuraiClone.ModelViews
{
    public class SearchModelView
    {
        [Required]
        public string ChooseEngine { get; set; }
        [Required]
        public string Query { get; set; }
    }
}