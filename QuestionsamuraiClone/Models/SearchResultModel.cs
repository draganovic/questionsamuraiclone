﻿namespace QuestionsamuraiClone.Models
{
    public class SearchResultModel
    {
        public SearchResultModel(string title, string url)
        {
            Title = title;
            URL = url;
        }

        public string Title { get; set; }
        public string URL { get; set; }
    }
}