﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore; 

namespace QuestionsamuraiClone.Models
{
    public class QuestionsamuraiDbContext : IdentityDbContext<ApplicationUser>
    {
        public QuestionsamuraiDbContext(DbContextOptions<QuestionsamuraiDbContext>options) : base(options)
        {
            
        }
    }
}