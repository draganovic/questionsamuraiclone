﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using QuestionsamuraiClone.Models;
using QuestionsamuraiClone.ModelViews;

namespace QuestionsamuraiClone.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
        
        public async Task<IActionResult> Search(SearchModelView searchModelView)
        {
            if (!ModelState.IsValid)
            {
                return View("Index",searchModelView);
            }
            var results = new List<String>();
            List<String> opirators = new List<string>()
            {
                "Por que",
                "Quando",
                "Qual",
                "Onde",
                "Como",
                "Como fazer",
                "Como pode",
                "Quanto custa",
                "Como seria",
                "Quem",
                "O que é",
                "O que deveria",
                "O que pode",
                "O que seria"
            };
            if (searchModelView.ChooseEngine == "Google")
            {
                await GetGoogle(searchModelView.Query, results);
                await GetGoogle(searchModelView.Query +"%20", results);

                foreach (var opirator in opirators)
                {
                    var o = opirator.Replace(" ", "%20");
                    await GetGoogle(o+"%20"+searchModelView.Query, results);
                }
            }
            else if (searchModelView.ChooseEngine == "Youtube")
            {
                await GetYoutube(searchModelView.Query, results);
                await GetYoutube(searchModelView.Query + "%20", results);

                foreach (var opirator in opirators)
                {
                    var o = opirator.Replace(" ", "%20");
                    await GetYoutube(o+"%20"+searchModelView.Query, results);
                }
            }
            return View(results);
        }

        private static async Task GetGoogle(string Query, List<string> results)
        {
            string url = $"https://www.google.com.br/complete/search?client=psy-ab&hl=pt-BR&q={Query}";
            using (var httpClient = new HttpClient())
            {
                var json = await httpClient.GetStringAsync(url);
                JArray a = JArray.Parse(json);
                JArray b = JArray.Parse(a[1].ToString());
                foreach (JArray o in b.Children<JArray>())
                {
                    Console.WriteLine(o[0].ToString());
                    results.Add(o[0].ToString().Replace("<b>", "").Replace("</b>", ""));
                }
            }
        }

        private static async Task GetYoutube(string Query, List<string> results)
        {
            string url = $"https://clients1.google.com/complete/search?hl=pt&gl=br&ds=yt&client=youtube&q={Query}";
            using (var httpClient = new HttpClient())
            {
                var json = await httpClient.GetStringAsync(url);
                int jsonStart = json.IndexOf("[");
                json = json.Substring(jsonStart);
                json = json.Substring(0, json.Length - 1);
                Console.WriteLine(json);
                JArray a = JArray.Parse(json);

                JArray b = JArray.Parse(a[1].ToString());
                foreach (JArray o in b.Children<JArray>())
                {
                    Console.WriteLine(o[0].ToString());
                    results.Add(o[0].ToString());
                }
            }
        }
    }
}