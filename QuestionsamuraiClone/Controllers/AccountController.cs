﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuestionsamuraiClone.Models;
using QuestionsamuraiClone.ModelViews;

namespace QuestionsamuraiClone.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(loginViewModel.UserName,loginViewModel.Password,loginViewModel.RememberMe,false);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Invalid Login Attempt.");
                return View(loginViewModel);
            }
            
            return View(loginViewModel);
        }

        public async Task<IActionResult> Logout() { 
            await _signInManager.SignOutAsync(); 
            return RedirectToAction("Index", "Home"); 
        } 
        
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel registerViewModel)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser {UserName = registerViewModel.UserName, Email = registerViewModel.Email};

                var userEmail = await _userManager.Users.Where(u => u.NormalizedEmail == user.Email.ToUpper()).FirstOrDefaultAsync();
                if (userEmail != null)
                {
                    ModelState.AddModelError("", "This Email already used.");
                    return View(registerViewModel);
                }

                var result = await _userManager.CreateAsync(user, registerViewModel.Password);
                
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, false);
                    return RedirectToAction("Index", "Home");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("",error.Description);
                }
            }
            return View(registerViewModel);
        }
    }
}